﻿using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Payments.BankDeposit.Models
{
    public class PaymentInfoModel : BaseNopModel
    {
        public string DescriptionText { get; set; }
    }
}