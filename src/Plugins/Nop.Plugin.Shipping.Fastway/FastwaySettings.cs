﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Shipping.Fastway
{
    public class FastwaySettings : ISettings
    {
        public string ApiKey { get; set; }

	    public string FranchiseCode { get; set; }

		public decimal AdditionalHandlingCharge { get; set; }
    }
}