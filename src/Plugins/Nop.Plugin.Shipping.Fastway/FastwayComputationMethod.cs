﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Routing;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Shipping;
using Nop.Core.Plugins;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Shipping;
using Nop.Services.Shipping.Tracking;

namespace Nop.Plugin.Shipping.Fastway
{
    /// <summary>
    /// Fastway computation method
    /// </summary>
    public class FastwayComputationMethod : BasePlugin, IShippingRateComputationMethod
    {
        #region Constants

        private const int MIN_LENGTH = 50; // 5 cm
        private const int MIN_WEIGHT = 500; // 500 g
        private const int MAX_LENGTH = 1050; // 105 cm
        private const int MAX_WEIGHT = 23000; // 23 Kg
        private const int ONE_KILO = 1000; // 1 kg
        private const int ONE_CENTIMETER = 10; // 1 cm

	    private const int THRESHOLD_BIG_PARCEL = 10;
	    private const int THRESHOLD_SMALL_PARCEL = 2;


        private const string GATEWAY_URL_DOMESTIC_ALLOWED_SERVICES = "https://nz.api.fastway.org/v2/psc/eta/";

        #endregion

        #region Fields

        private readonly ICurrencyService _currencyService;
        private readonly IMeasureService _measureService;
        private readonly IShippingService _shippingService;
        private readonly ISettingService _settingService;
        private readonly FastwaySettings _FastwaySettings;

        #endregion

        #region Ctor
        public FastwayComputationMethod(ICurrencyService currencyService, 
            IMeasureService measureService, IShippingService shippingService, 
            ISettingService settingService, FastwaySettings FastwaySettings)
        {
            this._currencyService = currencyService;
            this._measureService = measureService;
            this._shippingService = shippingService;
            this._settingService = settingService;
            this._FastwaySettings = FastwaySettings;
        }
        #endregion

        #region Utilities

        private MeasureWeight GatewayMeasureWeight
        {
            get
            {
                return this._measureService.GetMeasureWeightBySystemKeyword("grams");
            }
        }

        private MeasureDimension GatewayMeasureDimension
        {
            get
            {
                return this._measureService.GetMeasureDimensionBySystemKeyword("millimetres");
            }
        }

        private int GetWeight(GetShippingOptionRequest getShippingOptionRequest)
        {
            var totalWeigth = _shippingService.GetTotalWeight(getShippingOptionRequest);
            int value = Convert.ToInt32(Math.Ceiling(this._measureService.ConvertFromPrimaryMeasureWeight(totalWeigth, this.GatewayMeasureWeight)));
            return (value < MIN_WEIGHT ? MIN_WEIGHT : value);
        }

        private ShippingInfoByParcel RequestShippingOptions(string city, string toPostcode, decimal weight)
        {
            var shippingInfoByParcel = new ShippingInfoByParcel()
            {
                DeliveryTimeFrameDays = 0,
                EstimatedDeveliveryDate = DateTime.MaxValue,
                IsRural = false,
                ShippingInfoOptions = new List<ShippingInfoOption>() { new ShippingInfoOption() { BaseWeight = 0, Cost = 0, LabelColor = "", NumberOfExcessLabel =0, NumberOfRuralLabel =0, Price =0} },
            };
            if (string.IsNullOrWhiteSpace(city) || string.IsNullOrWhiteSpace(toPostcode)) return shippingInfoByParcel;
            var sb = new StringBuilder();

	        sb.AppendFormat(GATEWAY_URL_DOMESTIC_ALLOWED_SERVICES);
	        sb.AppendFormat("/{0}", _FastwaySettings.FranchiseCode);
	        sb.AppendFormat("/{0}", city);
	        sb.AppendFormat("/{0}", toPostcode);
	        sb.AppendFormat("/{0}", weight.Round(RoundingType.Rounding1Up));
	        sb.AppendFormat("?pickupDate={0:dd/MM/yyyy}&", DateTime.Today);
	        sb.AppendFormat("api_key={0}", _FastwaySettings.ApiKey);

            var request = WebRequest.Create(sb.ToString()) as HttpWebRequest;
            request.Method = "GET";
            Stream stream;

            try
            {
                var response = request.GetResponse();
                stream = response.GetResponseStream();
            }
            catch (WebException ex)
            {
                stream = ex.Response.GetResponseStream();
            }

            //parse json from response
            using (var reader = new StreamReader(stream))
            {
                var json = reader.ReadToEnd();
                if (!string.IsNullOrEmpty(json))
                {
                    var parsed = JObject.Parse(json);
                    JToken jToken;
                    try
                    {
	                    jToken = parsed["result"];

						if (jToken != null)
						{
							shippingInfoByParcel.IsRural = jToken["isRural"].Value<bool>();
							shippingInfoByParcel.DeliveryTimeFrameDays = jToken["delivery_timeframe_days"].Value<int>();
							shippingInfoByParcel.ShippingInfoOptions = new List<ShippingInfoOption>();

							var options = (JArray)(jToken["services"]);
                            foreach (var option in options)
                            {
	                            if (option["type"].Value<string>() == "Parcel")
	                            {
									var shippingInfoOption = new ShippingInfoOption();
		                            shippingInfoOption.Price = option["totalprice_normal"].Value<decimal>();
		                            shippingInfoOption.Cost = option["totalprice_frequent"].Value<decimal>();
		                            shippingInfoOption.NumberOfExcessLabel = option["excess_labels_required"].Value<int>();
		                            shippingInfoOption.NumberOfRuralLabel = option["rural_labels_required"].Value<int>();
		                            shippingInfoByParcel.ShippingInfoOptions.Add(shippingInfoOption);

								}
                                
                            }
                        }
                    }
                    catch (Exception)
                    {
                        //if the size or weight of the parcel exceeds the allowable, 
                        //and if for example the set(or not set) the wrong postal code, 
                        //the Fastway errorMessage returns the error text.
                        //As a result, the client can not use the services of the service
                        jToken = parsed["error"];
                        if (jToken != null)
                        {
                            var error = (JValue)jToken;
                            throw new NopException(error.Value.ToString());
                        }
                        throw new Exception("Shipping service is not valid, please contact with us.");
                    }
                }
                else
                {
                    throw new Exception("Shipping service is not valid, please contact with us.");
                }
            }
            return shippingInfoByParcel;
        }

        #endregion

        #region Methods

        /// <summary>
        ///  Gets available shipping options
        /// </summary>
        /// <param name="getShippingOptionRequest">A request for getting shipping options</param>
        /// <returns>Represents a response of getting shipping rate options</returns>
        public GetShippingOptionResponse GetShippingOptions(GetShippingOptionRequest getShippingOptionRequest)
        {
            if (getShippingOptionRequest == null)
                throw new ArgumentNullException("getShippingOptionRequest");

            var response = new GetShippingOptionResponse();

            if (getShippingOptionRequest.Items == null)
            {
                response.AddError("No shipment items");
                return response;
            }

            if (getShippingOptionRequest.ShippingAddress == null)
            {
                response.AddError("Shipping address is not set");
                return response;
            }

            if (string.IsNullOrEmpty(getShippingOptionRequest.ShippingAddress.ZipPostalCode))
            {
                response.AddError("Shipping zip (postal code) is not set");
                return response;
            }

	        var items = getShippingOptionRequest.Items.OrderByDescending(item=>item?.ShoppingCartItem?.Product?.Weight).ToList();

	        var totalRate = decimal.Zero;
	        var baseShippingInfoForBigParcel = RequestShippingOptions(getShippingOptionRequest.ShippingAddress.City,
		        getShippingOptionRequest.ShippingAddress.ZipPostalCode, THRESHOLD_BIG_PARCEL);
	        var baseRateForBigParcel = baseShippingInfoForBigParcel.ShippingInfoOptions.Min(s => s.Price);
	        var baseShippingInfoForSmallParcel = RequestShippingOptions(getShippingOptionRequest.ShippingAddress.City,
		        getShippingOptionRequest.ShippingAddress.ZipPostalCode, THRESHOLD_SMALL_PARCEL);
	        var baseRateForSmallParcel = baseShippingInfoForSmallParcel.ShippingInfoOptions.Min(s => s.Price);



	        var currentParcelWeight = decimal.Zero;
	        var numOfBigParcel = 0;
	        var numOfSmallParcel = 0;
	        for (int i = 0; i < items.Count; i++)
	        {
		        var item = items[i];
			    var weightPerItem = _shippingService.GetShoppingCartItemWeight(item.ShoppingCartItem);
				var qtyLeft = item.ShoppingCartItem.Quantity;
		        while (qtyLeft > 0)
		        {
			        if (weightPerItem >= THRESHOLD_BIG_PARCEL)
			        {
				        numOfBigParcel = numOfBigParcel + 1;
				        qtyLeft = qtyLeft - 1;
				        currentParcelWeight = 0;

			        }
			        else
			        {
						var weightLeft = THRESHOLD_BIG_PARCEL - currentParcelWeight;
				        if (weightPerItem >= weightLeft)
				        {
					        currentParcelWeight = 0;
					        numOfBigParcel = numOfBigParcel + 1;
					        qtyLeft = qtyLeft - 1;
				        }
				        else
				        {
							var allowQty = (int)Math.Floor(weightLeft / weightPerItem);
					        allowQty = allowQty > qtyLeft ? qtyLeft : allowQty;
					        currentParcelWeight = currentParcelWeight + weightPerItem * allowQty;
					        qtyLeft = qtyLeft - allowQty;
					        if (qtyLeft > 0)
					        {
						        numOfBigParcel = numOfBigParcel + 1;
						        currentParcelWeight = 0;
					        }
				        }
					}

				}
	        }
	        if (currentParcelWeight > THRESHOLD_SMALL_PARCEL)
	        {
		        numOfBigParcel = numOfBigParcel + 1;
	        }
	        else
	        {
		        numOfSmallParcel = numOfSmallParcel + 1;
	        }

	        totalRate = baseRateForBigParcel * numOfBigParcel + baseRateForSmallParcel * numOfSmallParcel;

            try
            {
                var shippingOption = new ShippingOption
                {
	                Description = string.Format("Delivery in {0} working days", baseShippingInfoForBigParcel.DeliveryTimeFrameDays),
					Name = "Courier",
					Rate = totalRate,
                };
				response.ShippingOptions.Add(shippingOption);
            }
            catch (NopException ex)
            {
                response.AddError(ex.Message);
                return response;
            }
            catch (Exception)
            {
                response.AddError("Fastway Service is currently unavailable, try again later");
                return response;
            }
            
            foreach (var shippingOption in response.ShippingOptions)
            {
                shippingOption.Rate += _FastwaySettings.AdditionalHandlingCharge;
            }
            return response;
        }

        /// <summary>
        /// Gets fixed shipping rate (if shipping rate computation method allows it and the rate can be calculated before checkout).
        /// </summary>
        /// <param name="getShippingOptionRequest">A request for getting shipping options</param>
        /// <returns>Fixed shipping rate; or null in case there's no fixed shipping rate</returns>
        public decimal? GetFixedRate(GetShippingOptionRequest getShippingOptionRequest)
        {
            return null;
        }

        /// <summary>
        /// Gets a route for provider configuration
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "ShippingFastway";
            routeValues = new RouteValueDictionary { { "Namespaces", "Nop.Plugin.Shipping.Fastway.Controllers" }, { "area", null } };
        }

        /// <summary>
        /// Install plugin
        /// </summary>
        public override void Install()
        {
            //settings
            var settings = new FastwaySettings
            {
                AdditionalHandlingCharge = 0
            };
            _settingService.SaveSetting(settings);

            //locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Fastway.Fields.ApiKey", "Fastway API Key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Fastway.Fields.ApiKey.Hint", "Specify Fastway API Key.");
	        this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Fastway.Fields.FranchiseCode", "Fastway Franchise Code");
	        this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Fastway.Fields.FranchiseCode.Hint", "Specify Fastway Franchise Code which is parcel send from.");
			this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Fastway.Fields.AdditionalHandlingCharge", "Additional handling charge");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Fastway.Fields.AdditionalHandlingCharge.Hint", "Enter additional handling fee to charge your customers.");

            base.Install();
        }

        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<FastwaySettings>();

			//locales
			this.DeletePluginLocaleResource("Plugins.Shipping.Fastway.Fields.ApiKey");
	        this.DeletePluginLocaleResource("Plugins.Shipping.Fastway.Fields.ApiKey.Hint");
	        this.DeletePluginLocaleResource("Plugins.Shipping.Fastway.Fields.FranchiseCode");
	        this.DeletePluginLocaleResource("Plugins.Shipping.Fastway.Fields.FranchiseCode.Hint");
			this.DeletePluginLocaleResource("Plugins.Shipping.Fastway.Fields.AdditionalHandlingCharge");
            this.DeletePluginLocaleResource("Plugins.Shipping.Fastway.Fields.AdditionalHandlingCharge.Hint");

            base.Uninstall();
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets a shipping rate computation method type
        /// </summary>
        public ShippingRateComputationMethodType ShippingRateComputationMethodType
        {
            get
            {
                return ShippingRateComputationMethodType.Realtime;
            }
        }

        /// <summary>
        /// Gets a shipment tracker
        /// </summary>
        public IShipmentTracker ShipmentTracker
        {
            get { return null; }
        }

        #endregion
    }

	#region Class

	public class ShippingInfoByParcel
	{
		public int DeliveryTimeFrameDays { get; set; }
		public DateTime EstimatedDeveliveryDate { get; set; }

		public bool IsRural { get; set; }
		
		public IList<ShippingInfoOption> ShippingInfoOptions { get; set; }
	}


	public class ShippingInfoOption
	{
		public decimal Price { get; set; }
		public decimal Cost { get; set; }
		public string LabelColor { get; set; }
		public int NumberOfExcessLabel { get; set; }
		public int NumberOfRuralLabel { get; set; }

		public int BaseWeight { get; set; }
	}
	#endregion
}