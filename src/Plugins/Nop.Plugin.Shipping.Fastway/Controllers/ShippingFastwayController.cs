﻿using System.Web.Mvc;
using Nop.Plugin.Shipping.Fastway.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Shipping.Fastway.Controllers
{
    [AdminAuthorize]
    public class ShippingFastwayController : BasePluginController
    {
        private readonly FastwaySettings _fastwaySettings;
        private readonly ISettingService _settingService;
        private readonly ILocalizationService _localizationService;

        public ShippingFastwayController(FastwaySettings FastwaySettings,
            ISettingService settingService,
            ILocalizationService localizationService)
        {
            this._fastwaySettings = FastwaySettings;
            this._settingService = settingService;
            this._localizationService = localizationService;
        }

        [ChildActionOnly]
        public ActionResult Configure()
        {
            var model = new FastwayShippingModel();
            model.ApiKey = _fastwaySettings.ApiKey;
            model.ApiKey = _fastwaySettings.FranchiseCode;
            model.AdditionalHandlingCharge = _fastwaySettings.AdditionalHandlingCharge;

            return View("~/Plugins/Shipping.Fastway/Views/Configure.cshtml", model);
        }

        [HttpPost]
        [ChildActionOnly]
        public ActionResult Configure(FastwayShippingModel model)
        {
            if (!ModelState.IsValid)
            {
                return Configure();
            }
            
            //save settings
            _fastwaySettings.ApiKey = model.ApiKey;
            _fastwaySettings.AdditionalHandlingCharge = model.AdditionalHandlingCharge;
            _fastwaySettings.FranchiseCode = model.FranchiseCode;
            _settingService.SaveSetting(_fastwaySettings);

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }

    }
}
