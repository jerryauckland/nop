﻿Group: Shipping rate computation
FriendlyName: Fastway
SystemName: Shipping.Fastway
Version: 1.20
SupportedVersions: 3.90
Author: nopCommerce team
DisplayOrder: 1
FileName: Nop.Plugin.Shipping.Fastway.dll
Description: This plugin offers shipping rates by Fastway