﻿using Nop.Web.Framework;

namespace Nop.Plugin.Shipping.Fastway.Models
{
    public class FastwayShippingModel
    {
        [NopResourceDisplayName("Plugins.Shipping.Fastway.Fields.ApiKey")]
        public string ApiKey { get; set; }

	    [NopResourceDisplayName("Plugins.Shipping.Fastway.Fields.FranchiseCode")]
	    public string FranchiseCode { get; set; }

		[NopResourceDisplayName("Plugins.Shipping.Fastway.Fields.AdditionalHandlingCharge")]
        public decimal AdditionalHandlingCharge { get; set; }
    }
}