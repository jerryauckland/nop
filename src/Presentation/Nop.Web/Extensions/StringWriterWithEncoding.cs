﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Nop.Web.Extensions
{
    public sealed class StringWriterWithEncoding : StringWriter
    {
        public StringWriterWithEncoding() : this(Encoding.UTF8) { }

        public StringWriterWithEncoding(Encoding encoding)
        {
            this.Encoding = encoding;
        }

        public override Encoding Encoding { get; }
    }
}